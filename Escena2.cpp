#include "Escena2.h"
#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"

USING_NS_CC;


Scene* Escena2::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = Escena2::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool Escena2::init()
{
	if (!Layer::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto boton = MenuItemImage::create("im/Flecha.png","im/Flecha.png", CC_CALLBACK_1(Escena2::menuCloseCallback, this));
	boton->setPosition(Vec2(origin.x + boton->getContentSize().width / 2, origin.y + visibleSize.height - boton->getContentSize().height / 2));
	boton->setScale(0.5);
	auto menu = Menu::create(boton, NULL);
	this->addChild(menu, 1);

	auto fondo = Sprite::create("im/Fondo.png");
	fondo->setPosition(Vec2::ZERO);
	this->addChild(fondo, 0);

	return true;
}

void Escena2::menuCloseCallback(Ref* pSender)
{
	auto cambio = HelloWorld::createScene();
	Director::getInstance()->replaceScene(cambio);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}
