#include "HelloWorldScene.h"
#include "Escena2.h"
#include "Mal.h"
#include "SimpleAudioEngine.h"
#include "cocos2d.h"
USING_NS_CC;

Scene* Mal::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = Mal::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool Mal::init()
{
	//////////////////////////////
	// 1. super init first
	if (!Layer::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	/////////////////////////////
	// 2. add a menu item with "X" image, which is clicked to quit the program
	//    you may modify it.

	// add a "close" icon to exit the progress. it's an autorelease object
	auto closeItem = MenuItemImage::create(
		"im/flechaca.png", "im/flechaca.png",
		CC_CALLBACK_1(Mal::menuCloseCallback, this));

	closeItem->setPosition(Vec2(origin.x + visibleSize.width / 4, origin.y + visibleSize.height / 1.6));
	closeItem->setScale(2.0f);



	// create menu, it's an autorelease object
	auto menu = Menu::create(closeItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);

	/////////////////////////////
	// 3. add your codes below...


	// add a label shows "Hello World"
	// create and initialize a label

	label = Label::createWithTTF("CRAZY ROAD", "fonts/RaceFlow.ttf", 120);

	// position the label on the center of the screen
	label->setPosition(Vec2(origin.x + visibleSize.width / 2,
		origin.y + visibleSize.height / 1.25));
	label->setColor(ccc3(239, 255, 000));


	// add the label as a child to this layer
	this->addChild(label, 1);

	auto facil = Label::createWithTTF("Jugar", "fonts/Marker Felt.ttf", 40);
	facil->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 1.65));
	facil->setColor(ccc3(146, 043, 062));
	this->addChild(facil, 1);
	// add "HelloWorld" splash screen"


	// position the sprite on the center of the screen

	auto back = Sprite::create("im/fondo_principal.jpg");
	back->setPosition(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2);
	back->setScale(2);
	this->addChild(back, 0);

	this->scheduleUpdate();
	return true;
}


void Mal::menuCloseCallback(Ref* pSender)
{
	auto cambio = Mal::createScene();
	Director::getInstance()->replaceScene(cambio);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}




