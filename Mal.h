#ifndef __MAL_SCENE_H__
#define __MAL_SCENE_H__

#include "cocos2d.h"


class Mal : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	// a selector callback
	void menuCloseCallback(cocos2d::Ref* pSender);

private:
	float m_timeCounter = 0.0;
	cocos2d::Label*label;


	// implement the "static create()" method manually
	CREATE_FUNC(Mal);
};

#endif // __HELLOWORLD_SCENE_H__