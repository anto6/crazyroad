#ifndef __MEDIA_SCENE_H__
#define __MEDIA_SCENE_H__
#include "cocos2d.h"
#include <vector>
#include "Enemy.h"

class Media : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	void menuCloseCallback(cocos2d::Ref* pSender);
	virtual bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *unused_event);
	virtual void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *unused_event);
	virtual void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *unused_event);
	CREATE_FUNC(Media);

private:

	void initTouchListener();
	int anchura = 0;
	int altura = 0;
	cocos2d::Sprite* coche;
	cocos2d::Label* label;
	cocos2d::CCSprite* enemigo;
	cocos2d::Sprite* fondo;
	std::vector <Enemy*> enemigos_vector;
	int posicion = 0;
	float m_timeCounter = 0.0f;
	void sacamalo(float dt);
	void scroll();
	void update(float dt);
	void derrota();
	void victoria();


};

#endif