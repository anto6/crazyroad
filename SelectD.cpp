#include "HelloWorldScene.h"
#include "Escena2.h"
#include "Mal.h"
#include "SimpleAudioEngine.h"
#include "SelectD.h"
USING_NS_CC;

Scene* SelectD::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = SelectD::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

// on "init" you need to initialize your instance
bool SelectD::init()
{
	
	if (!Layer::init())
	{
		return false;
	}

	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();

	auto closeItem = MenuItemImage::create(
		"im/playbutton.png", "im/playbutton_pulsado.png",
		CC_CALLBACK_1(SelectD::menuCloseCallback, this));

	closeItem->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 1.8));
	closeItem->setScale(0.3f);

	auto botonsalida = MenuItemImage::create(
		"im/exit.png", "im/exit_pulsado.png",
		CC_CALLBACK_1(SelectD::menuCloseCallbackexit, this));

	botonsalida->setPosition(Vec2(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2.5));
	botonsalida->setScale(0.3f);

	// implementación de botones dentro del menú
	auto menu = Menu::create(closeItem, NULL);
	menu->setPosition(Vec2::ZERO);
	this->addChild(menu, 1);

	auto menu2 = Menu::create(botonsalida, NULL);
	menu2->setPosition(Vec2::ZERO);
	this->addChild(menu2, 1);


	label = Label::createWithTTF("CHOOSE DIFICULTY", "fonts/RaceFlow.ttf", 120);

	label->setPosition(Vec2(origin.x + visibleSize.width / 2,
		origin.y + visibleSize.height / 1.25));
	label->setColor(ccc3(239, 255, 000));


	// add the label as a child to this layer
	this->addChild(label, 1);


	auto back = Sprite::create("im/levelselect.jpg");
	back->setPosition(origin.x + visibleSize.width / 2, origin.y + visibleSize.height / 2);
	back->setScale(0.75);
	this->addChild(back, 0);

	this->scheduleUpdate();
	return true;
}


void SelectD::menuCloseCallback(Ref* pSender)
{
	auto cambio = Escena2::createScene();
	Director::getInstance()->replaceScene(cambio);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

void SelectD::menuCloseCallbackexit(Ref* pSender)
{
	Director::getInstance()->end();

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}
