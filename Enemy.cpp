#include "Enemy.h"
#include "SimpleAudioEngine.h"
#include "stdlib.h"

USING_NS_CC;

Enemy* Enemy::create(const std::string& fileName)
{
	Enemy* cochemalo = new Enemy();
	if (cochemalo && cochemalo->initWithFile(fileName))
	{
		cochemalo->autorelease();
	}
	else
	{
		CC_SAFE_RELEASE_NULL(cochemalo);
	}

	return cochemalo;
}

bool Enemy::initWithFile(const std::string& spriteName)
{
	if (!Sprite::initWithFile(spriteName))
	{
		return false;
	}
	return true;
}

void Enemy::spawnEnemigo()
{
	Size winSize = Director::getInstance()->getWinSize();
	int minX = this->getContentSize().width / 2;
	int maxX = winSize.height - this->getContentSize().width / 2;
	int rangoX = maxX - minX;
	int actualX = (rand() % rangoX) + minX;
	this->setPosition(ccp(actualX, winSize.width + (this->getContentSize().width / 2)));
	int duracionmin = (int)2.0;
	int duracionmax = (int)8.0;
	int rangeDuration = duracionmax - duracionmin;
	int actualDuration = (rand() % rangeDuration) + duracionmin;

	CCFiniteTimeAction* actionMove =
		CCMoveTo::create((float)actualDuration,
			ccp(actualX, 0 - this->getContentSize().width / 2));
	CCFiniteTimeAction* actionMoveDone =
		CCCallFuncN::create(this,
			callfuncN_selector(Enemy::spriteMoveFinished));
	this->runAction(CCSequence::create(actionMove,
		actionMoveDone, NULL));
}

void Enemy::spriteMoveFinished(Node* sender)
{
	CCSprite *sprite = (CCSprite *)sender;
	this->removeChild(sprite, true);
}


