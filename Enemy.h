#ifndef __ENEMY_SPRITE_H__
#define __ENEMY_SPRITE_H__

#include "cocos2d.h"

class Enemy : public cocos2d::Sprite
{
public:
	static Enemy* create(const std::string& fileName);

	virtual bool initWithFile(const std::string&) override;
	void spawnEnemigo();
	void spriteMoveFinished(Node* sender);
	

};

#endif 