#ifndef __ESCENA2_SCENE_H__
#define __ESCENA2_SCENE_H__

#include "cocos2d.h"

class Escena2 : public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene();
	virtual bool init();
	void menuCloseCallback(cocos2d::Ref* pSender);
	CREATE_FUNC(Escena2);
};

#endif
