#include "Enemy.h"
#include "Hardcore.h"
#include "Mal.h"
#include "HelloWorldScene.h"
#include "SimpleAudioEngine.h"
#include "Victory.h"
#include "time.h"
#include "stdlib.h"
#include "string"
#include "cmath"
USING_NS_CC;

Scene* Hardcore::createScene()
{
	// 'scene' is an autorelease object
	auto scene = Scene::create();

	// 'layer' is an autorelease object
	auto layer = Hardcore::create();

	// add layer as a child to scene
	scene->addChild(layer);

	// return the scene
	return scene;
}

bool Hardcore::init()
{
	if (!Layer::init())
	{
		return false;
	}
	CocosDenshion::SimpleAudioEngine::sharedEngine()->playBackgroundMusic(
		"im/doomusic.mp3", false);
	auto visibleSize = Director::getInstance()->getVisibleSize();
	Vec2 origin = Director::getInstance()->getVisibleOrigin();
	anchura = visibleSize.width;
	altura = visibleSize.height;
	label = Label::createWithTTF("", "fonts/Calculator.ttf", 35);
	label->setPosition(Vec2(anchura / 1.2, altura / 1.1));
	label->setColor(ccc3(17, 223, 2));
	this->addChild(label, 2);
	//sprite del coche principal
	coche = Sprite::create("im/main_car.png");
	coche->setScale(0.7);
	coche->setPosition(origin.x + visibleSize.width / 2, origin.y + coche->getContentSize().height / 2);
	this->addChild(coche, 1);
	posicion = coche->getPositionX();
	initTouchListener();
	this->scheduleUpdate();
	srand(time(NULL));
	fondo = Sprite::create("im/carreterainfernal.png");
	fondo->setPosition(origin.x + visibleSize.width / 2, origin.y + visibleSize.height*4.5);
	fondo->setScale(2.5f);
	this->scroll();
	this->addChild(fondo, 0);
	this->schedule(schedule_selector(Hardcore::sacamalo), 0.7);
	return true;
}

void Hardcore::menuCloseCallback(Ref* pSender)
{
	auto cambio = Mal::createScene();
	Director::getInstance()->replaceScene(cambio);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

bool Hardcore::onTouchBegan(Touch *touch, Event *unused_event)
{
	Vec2 position = touch->getLocation();

	if (position.x > coche->getPositionX())
	{
		auto movimiento = MoveBy::create(2, Vec2(20, 0));
		coche->runAction(movimiento);
	}

	else
	{
		auto movimiento = MoveBy::create(2, Vec2(-20, 0));
		coche->runAction(movimiento);
	}

	return true;
}

void Hardcore::onTouchMoved(Touch *touch, Event *unused_event)
{
	Vec2 position = touch->getLocation();
	if (position.x > coche->getPositionX())
	{
		auto movimiento = MoveBy::create(2, Vec2(20, 0));
		coche->runAction(movimiento);
	}

	else
	{
		auto movimiento = MoveBy::create(2, Vec2(-20, 0));
		coche->runAction(movimiento);
	}
}

void Hardcore::onTouchEnded(Touch *touch, Event *unused_event)
{

}

void Hardcore::initTouchListener()
{
	EventListenerTouchOneByOne* listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CC_CALLBACK_2(Hardcore::onTouchBegan, this);
	listener->onTouchMoved = CC_CALLBACK_2(Hardcore::onTouchMoved, this);
	listener->onTouchEnded = CC_CALLBACK_2(Hardcore::onTouchEnded, this);

	_eventDispatcher->addEventListenerWithSceneGraphPriority(listener, this);
}

//Algoritmo para decidir y cear un coche de manera aleatoria
void Hardcore::sacamalo(float dt)
{
	//Numero aleatorio para escojer el tipo de coche malo (4 tipos)
	int i;
	i = rand() % 7;
	Enemy *enemigo = NULL;
	switch (i)
	{
	case 0: enemigo = Enemy::create("im/Ambulance.png");
		enemigo->initWithFile("im/Ambulance.png");
		enemigo->setScale(0.9);
		break;
	case 1: enemigo = Enemy::create("im/Gmail/taxi.png");
		enemigo->initWithFile("im/Gmail/taxi.png");
		enemigo->setScale(0.8);
		break;
	case 2: enemigo = Enemy::create("im/Gmail/truck.png");
		enemigo->initWithFile("im/Gmail/truck.png");
		enemigo->setScale(1.7);
		break;
	case 3: enemigo = Enemy::create("im/Gmail/police.png");
		enemigo->initWithFile("im/Gmail/Police.png");
		enemigo->setScale(0.8);
		break;
	case 4: enemigo = Enemy::create("im/Gmail/van.png");
		enemigo->initWithFile("im/Gmail/van.png");
		enemigo->setScale(0.9);

	case 5: enemigo = Enemy::create("im/Gmail/car1.png");
		enemigo->initWithFile("im/Gmail/van.png");
		enemigo->setScale(0.9);

	case 6: enemigo = Enemy::create("im/Gmail/mini_truck.png");
		enemigo->initWithFile("im/Gmail/van.png");
		enemigo->setScale(0.9);

	}

	enemigos_vector.push_back(enemigo);
	this->addChild(enemigo, 1);
	enemigo->spawnEnemigo();

}

void Hardcore::scroll()
{
	auto scrollv = MoveBy::create(500, Vec2(0, -40000));
	fondo->runAction(scrollv);

}

void Hardcore::update(float dt)
{
	m_timeCounter += dt;

	//detector de colisiones
	for (unsigned i = 0; i < enemigos_vector.size(); i++)
	{
		Rect poscoche = coche->getBoundingBox();
		Rect posenemigo = enemigos_vector.at(i)->getBoundingBox();
		if (posenemigo.intersectsRect(poscoche))
		{
			this->derrota();
		}
	}

	//intento de soluci�n al bug de los coches montados
	for (unsigned i = 0; i < enemigos_vector.size();i++)
	{
		for (unsigned j = i + 1; j < enemigos_vector.size(); j++)
		{
			auto pos1 = enemigos_vector.at(i)->getPositionY();
			auto pos2 = enemigos_vector.at(j)->getPositionY();
			auto resta = pos2 - pos1;
			if (abs(resta) < 15.0f)
			{
				auto movimiento = MoveBy::create(2, Vec2(20, 0));
				enemigos_vector.at(j)->runAction(movimiento);
			}
		}
	}

	if (m_timeCounter > 40.0f)
	{
		this->victoria();
	}

	if ((coche->getPositionX() < 0.0 || coche->getPositionX() > anchura))
	{
		this->derrota();
	}

	label->setString(std::to_string(m_timeCounter));
}

void Hardcore::derrota()
{
	auto cambio = Mal::createScene();
	Director::getInstance()->replaceScene(cambio);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}

void Hardcore::victoria()
{
	auto cambio = Victory::createScene();
	Director::getInstance()->replaceScene(cambio);

#if (CC_TARGET_PLATFORM == CC_PLATFORM_IOS)
	exit(0);
#endif
}
